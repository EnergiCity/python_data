import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime
from datetime import timedelta
import psycopg2
import os

start_date = '2021-07-01'
#'postgres://deploy:f35a1d8e2c1cb2b0749588361facv34f@165.22.69.62:5432/energicity_production'
READONLY_URL = "postgresql://read_only:vt39v62tz5toajs3@energicity-production-database-do-user-7511625-0.a.db.ondigitalocean.com:25060/energicity_production?sslmode=require"
connection = psycopg2.connect(READONLY_URL)

def customer_daily_data_pull(date):
    day = date
    #this pulls the data from the database just like PGAdmin will do
    df = pd.read_sql(f'''
    SELECT a.id as "Customer ID",
        CONCAT('#', a.customer_id) "Account Num",
        c.meter_id as "Meter Number",
        (SELECT CASE WHEN c.middle_name IS NULL OR TRIM(c.middle_name) = '' THEN CONCAT(c.first_name,' ', c.last_name )
        ELSE CONCAT(c.first_name,' ', c.middle_name,' ', c.last_name ) END) as "Customer Name",
        b. name as "Project Name",
        a.date as "Date",
        a.usage as "Usage",
        a.price as "Price",
        a.balance_after as "Balance After",
        c.type, 
        c.manual_meter_update, 
        c.longitude, 
        c.latitude 
    from daily_consumptions a, projects b, customers c
    WHERE  a.date = '{date}' and 
        b.id=a.project_id and 
        b.id=c.project_id and
        a.customer_id=c.id and 
        c.meter_id is not NULL 
    ORDER BY a.date DESC;
    ''', con=connection)
    df['Date'] = df['Date'].astype('datetime64[ns]')
    return df


file_folder = '../data/customer_data/'
date_dt = pd.to_datetime(start_date)



date_dt = pd.to_datetime(start_date)
while (date_dt<datetime.datetime.now()):
    print (date_dt.strftime("%Y%m%d"))
    if date_dt>datetime.datetime.now()-timedelta(5):
        print ('pulling customer data')
        df = customer_daily_data_pull(date_dt.strftime("%Y%m%d"))
        file_name = date_dt.strftime("%Y%m%d")+'.csv' 
        if(len(df.index)>0):
            df.to_csv(file_folder+file_name)
        else:
            print ("no data for ", date_dt)        
    else:
        try:
            file_name = date_dt.strftime("%Y%m%d")+'.csv'
            df=pd.read_csv(file_folder+file_name)
        except:
            print ('pulling customer data')
            df = customer_daily_data_pull(date_dt.strftime ("%Y%m%d"))
            file_name = date_dt.strftime ("%Y%m%d")+'.csv'
            if(len(df.index)>0):
                df.to_csv(file_folder+file_name)
            else:
                print ("no data for ", date_dt)
    date_dt = date_dt+timedelta(days=1)
    print(f"the current date is {datetime.datetime.now()} and incremental date is {date_dt}, less than {date_dt < datetime.datetime.now()}")
    
connection.close()
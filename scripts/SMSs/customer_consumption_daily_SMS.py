def smss_text(customers_ids):

  # install the important information
  from pandas.io.parquet import get_engine
  from pandas.tseries.offsets import Day
  import psycopg2
  import pandas as pd
  import numpy as np
  from datetime import date, timedelta
  import requests as req


  #connect to the database:
  con = psycopg2.connect(database="energicity_production", user="read_only", password="vt39v62tz5toajs3", host="energicity-production-database-do-user-7511625-0.a.db.ondigitalocean.com", port="25060")
  print("Database opened successfully")

  #define the dates that will be used in the database query and the sms outputs
  today = date.today()
  yesterday = today - timedelta(days=1)

  # ask if you want to send the SMSs automaticlly to the customers:
  automatic_send = input("do you want to send SMSs automaticlly to customers \nYes: will generate the SMS and send it automaticlly \nNo: will just print the sms to console\n")

  # define the SMSs list to store all of the SMSs and return it.
  smss_text = pd.DataFrame(columns=['name', 'sms'])

  #loop over the customer ids to get their usage from the database
  for customer in customers_ids:
    customer_details = pd.read_sql(f'''
      select c.primary_phone phone,
        c.first_name "name",
        dc.total_amount charges,
        dc.usage usage,
        p.daily_minimum daily_min,
        dc.financial_plan_amount leasing_charges,
        dc.balance_after balance_after
      from customers c
          inner join daily_consumptions dc on c.id = dc.customer_id
          inner join projects p on c.project_id = p.id
      where c.id = '{customer}'
      and dc.date::date = '{yesterday}'
      ''', con=con)
    
    # check if there is the billing for yesterday went out
    if customer_details.shape[0] == 0:
      print(f"the charges for {yesterday.strftime('%d-%m-%Y')} didn't run yet, that why you can't see the SMSs links")
      break

    else:
      # store the values of each customer is their values
      name = customer_details['name'][0]
      phone = customer_details['phone'][0].replace("-", "")[-8:]
      charges = customer_details['charges'][0]
      usage = customer_details['usage'][0]
      daily_min = customer_details['daily_min'][0]
      leasing_charges = customer_details['leasing_charges'][0]
      balance_after = round(customer_details['balance_after'][0], 2)

      sms_text = f'''http://35.156.141.49:13013/cgi-bin/sendsms?username=kannel&password=kannel&to=232{phone}&dlr-mask=0&text=Hi+{name},+You+have+been+charged+{charges}+SLL+on+{yesterday.strftime('%d-%m-%Y')}.+It+corresponds+to+{usage}+kwh+of+electricity+use,+include+{daily_min}+of+service+charge,+and+{leasing_charges}+of+leasing+charge.+Your+new+balance+is+{balance_after}+SLL.'''
      sms_text = sms_text.replace(" ", "")
      smss_text.loc[len(sms_text)] = [name, sms_text]

      

      if automatic_send == "yes" or automatic_send == "Yes":
        get_res = req.get(url=sms_text)
        print(f"Message send to {name}")
      else: 
        print(f"SMS for {name}: \n{sms_text}")


  return smss_text

# customer list by their ID
CUSTOMER_ID_LIST = ['144888129', '480920296', '327552053', '966132732', '198302766', '306236350', '424586456', '306210791', '336452145', '135469687']

smss_dataframe = smss_text(CUSTOMER_ID_LIST)
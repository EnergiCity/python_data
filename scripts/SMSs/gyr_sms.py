# install the important information
import os
import psycopg2
import pandas as pd
from datetime import date, timedelta
from powerleone import sending_sms
from dotenv import load_dotenv

load_dotenv()

today = date.today()
yesterday = today - timedelta(days=2)


#connect to the database:
con = psycopg2.connect(database="energicity_production", user="read_only", password="vt39v62tz5toajs3", host="energicity-production-database-do-user-7511625-0.a.db.ondigitalocean.com", port="25060")
print("Database opened successfully")



# Text and Color experiemtnt for Foredugu customers

# customer list by their ID for text and color
CUSTOMER_ID_LIST_COLOR_TEXT = ['699066161', '308833230', '439873862', '767886360', '078644764', '340049962', '019506875', '312944503', '980818405', '193414751', '336452145']

#CUSTOMER_ID_LIST_COLOR_TEXT = ['336452145']

for id in CUSTOMER_ID_LIST_COLOR_TEXT:
    customer_details = pd.read_sql(f"""
    select c.primary_phone phone,
        concat(first_name, ' ', middle_name, ' ', last_name) as "name",
        dc.total_amount charges,
        dc.usage usage,
        p.daily_minimum daily_min,
        dc.financial_plan_amount leasing_charges,
        dc.balance_after balance_after,
        c.days_left days
      from customers c
          inner join daily_consumptions dc on c.id = dc.customer_id
          inner join projects p on c.project_id = p.id
      where c.id = '{id}'
      and dc.date::date = '{yesterday}'
    """, con=con)
    phone_number = None
    try:
        phone_number = customer_details["phone"][0].replace("-", "")
    except Exception as e:
        print(f"{e}\n the charges didn't run for {yesterday}")
        exit()
    name = customer_details["name"][0]
    charges = int(customer_details["charges"][0])
    usage = round(customer_details["usage"][0], 4)
    daily_min = customer_details["daily_min"][0]
    leasing_charges = customer_details["leasing_charges"][0]
    balance = int(customer_details["balance_after"][0])
    days = customer_details["days"][0]
    color = None

    if days > 2:
        color = 'Green'
    elif days > 0.5:
        color = 'Yellow'
    else:
        color = 'Red'

    sms_body = f"""{color}\nHi {name}, you have been charged {charges} SLL on {yesterday.strftime('%d-%m-%Y')}.\nIt corresponds to {usage} kWh of electricity use, including {daily_min} of service charge, and {leasing_charges} of leasing charge.\nYour new balance is {balance} SLL."""
    # I need to chage the phone number with the real customer's phone number
    sms = sending_sms(kannel_username=os.getenv("KANNEL_USER"), kannel_password=os.getenv("KANNEL_PASSWORD"), body=sms_body ,phone_number=phone_number)
    print(sms.url)


#Just color experiment for customers in Mange
# Just color customers in Mange
CUSTOMER_ID_LIST_COLOR = ['327552053', '480920296', '203102220', '966132732', '198302766', '898043639', '861561326','414229365', '551605661', '144888129', '336452145']

#CUSTOMER_ID_LIST_COLOR = ['336452145']


for id in CUSTOMER_ID_LIST_COLOR_TEXT:
    customer_details = pd.read_sql(f'''
        select concat(first_name, ' ', middle_name, ' ', last_name) as "name",
                primary_phone as "phone",
                days_left "days"
            from customers
            where id='{id}'
        ''', con=con)
    days = None
    try:
        days = customer_details.days.values[0]
    except Exception as e:
        print(f"{e}\n the charges didn't run for {yesterday}")
        exit()
    phone = customer_details.phone.values[0].replace("-", "")
    color = None

    #check which color should assign to the customer
    if days > 2:
        color = 'Green'
    elif days > 0.5:
        color = 'Yellow'
    else:
        color = 'Red'

    # I need to chage the phone number with the real customer's phone number
    sms = sending_sms(kannel_username=os.getenv("KANNEL_USER"), kannel_password=os.getenv("KANNEL_PASSWORD"), body=f"{color}" ,phone_number=phone)
    print(sms.url)






# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# -*- coding: utf-8 -*-
"""
Created on Thu May 24 16:18:13 2018

@author: joeph
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime
from datetime import timedelta
import psycopg2
import os

start_date = '2021-07-01'
#'postgres://deploy:f35a1d8e2c1cb2b0749588361facv34f@165.22.69.62:5432/energicity_production'
READONLY_URL = "postgresql://read_only:vt39v62tz5toajs3@energicity-production-database-do-user-7511625-0.a.db.ondigitalocean.com:25060/energicity_production?sslmode=require"
connection = psycopg2.connect(READONLY_URL)

def meter_one_day_data_pull(date):
    day = date
    next_day = (pd.to_datetime(date)+timedelta(days=1)).strftime ("%Y-%m-%d")
    print('about to pull data......')
    query = "SELECT meter_id, measured_at, usage as usage_in_kwh, 1000*1000*usage/230 as usage_in_milliamp_hours, \"on\" FROM onsite_readings WHERE measured_at > %s AND measured_at < %s ORDER BY measured_at;"
    args = (day, next_day)
    cr = connection.cursor()
    cr.execute(query, args)
    tmp=cr.fetchall()
    col_names = []
    for elt in cr.description:
        col_names.append(elt[0])
    df = pd.DataFrame(tmp, columns=col_names)
    df.set_index('measured_at', inplace=True)
    return df



file_folder = "../data/meter_data/"


date_dt = pd.to_datetime(start_date)
while (date_dt<datetime.datetime.now()):
    print (date_dt.strftime ("%Y%m%d"))
    try:
        file_name = date_dt.strftime ("%Y%m%d")+'.csv'
        df=pd.read_csv(file_folder+file_name, index_col=0, parse_dates=True)
        if(df.index[-1:].hour<23):
            print ('repulling data')
            df = meter_one_day_data_pull(date_dt.strftime ("%Y%m%d"))
            file_name = date_dt.strftime ("%Y%m%d")+'.csv'
            df.to_csv(file_folder+file_name)
            if(len(df.index)>0):
                df.to_csv(file_folder+file_name)
            else:
                print ("no data for ", date_dt-timedelta(1))

            
    except:
        print(os.getcwd())
        print ('pulling data')
        df = meter_one_day_data_pull(date_dt.strftime ("%Y%m%d"))
        file_name = date_dt.strftime("%Y%m%d")+'.csv'
        df.to_csv(file_folder+file_name)

                    
    date_dt = date_dt+timedelta(days=1)
    
connection.close()

# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file
"""

#%%
# import packages and define base functions for pulling data
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import datetime
from datetime import timedelta
from matplotlib.dates import DateFormatter

import psycopg2

pd.set_option('precision', 4)
    

def meter_data_pull(first_date, last_date):
    # function to pull meter data from stored data 
    file_folder_base = '../data/meter_data/'
    first_date = pd.to_datetime(first_date)
    last_date = pd.to_datetime(last_date)
    df_a = pd.read_csv(file_folder_base+first_date.strftime('%Y%m%d')+'.csv')
    date=first_date+timedelta(days=1)
    while date <= last_date:
        print(f"meter pulling date {date}")
        df_b = pd.read_csv(file_folder_base+date.strftime('%Y%m%d')+'.csv')
        df_a = df_a.append(df_b)
        date=date+timedelta(days=1)
    df_a['measured_at']=pd.to_datetime(df_a['measured_at'])
    # df_a.set_index('measured_at',inplace=True)
    return df_a

def customer_data_pull(first_date, last_date):
    file_folder_base = '../data/customer_data/'
    first_date = pd.to_datetime(first_date)
    last_date = pd.to_datetime(last_date)
    df_a = pd.read_csv(file_folder_base+first_date.strftime('%Y%m%d')+'.csv')
    date=first_date+timedelta(days=1)

    while date <= last_date:
        print(f"custoemr pulling date {date}")
        df_b = pd.read_csv(file_folder_base+date.strftime('%Y%m%d')+'.csv')
        df_a = df_a.append(df_b)
        date=date+timedelta(days=1)
    return df_a

#%%
# Code will run until yesterday / pull date today and find date yesterday in string format
tt=datetime.datetime.now()   
today = tt.strftime("%Y%m%d")

yesterday = (tt-timedelta(days=1)).strftime("%Y-%m-%d")

#%% pull meter data 
first_date_str = '2021-07-01'
df_a = meter_data_pull(first_date_str, yesterday)

#%% pull customer data 
check = 0
while check == 0:
    try: 
        df_customer_info = customer_data_pull(yesterday, yesterday)
        check = 1
    except:
        tt = (tt-timedelta(days=1))
        yesterday = tt.strftime("%Y-%m-%d")
        print(f"while except date {yesterday}")

df_customer_charge_pull = customer_data_pull(first_date_str, yesterday)

# get first date from which to pull data and get string 

df_customer_info.set_index('Account Num',inplace=True)

#%% Get meter project list
#project_array = ['Mano Dasse', 'Petifu Junction']

project_array = ['Conakry Dee', 'Kychom', 'Petifu Junction', 'WP1 CLINICS', 'Barmoi Munu', 'Malekuray', 'Shenge', 'Gbangbatoke', 'Kamasundo', 'Masimera', 'Sembehun', 'Senehun', 'Mafaray-Gbalamuya', 'Mano Dasse', 'Mokolleh', 'Kangahun', 'Njama', 'Bradford', 'Bomotoke', 'Kamasasa','Foredugu', 'Mange', 'Bauya', 'Sendugu', 'Madina Junction']
project_array.sort()

for project_name in project_array:
    
    df_customer_project= df_customer_info.loc[df_customer_info['Project Name']==project_name, :]
    df_customer_project.loc[:, 'customer num'] = df_customer_project.index
    df_customer_project.set_index('Meter Number', inplace=True)
    meter_list_a = np.array(df_customer_project.index.dropna())
        
    #%% calculate meter data based on meter reads
    df_a = df_a.drop_duplicates(subset=['measured_at', 'meter_id'])
    df_b = df_a[df_a['meter_id'].isin(meter_list_a)]
    
    df_meter = df_b.pivot(index='measured_at', columns = 'meter_id', values='usage_in_kwh')
    df_meter = df_meter.resample('15min').max()
    df_meter_a = df_meter.interpolate(method='time', limit=100000, limit_direction='backward')
    df_day = df_meter_a.resample('d').max()
    df_daily_energy = df_day.diff()
    df_daily_energy[df_daily_energy<0]=0
    #estimated = df_daily_energy.loc[yesterday, :].isnull()           
    df_daily_energy.fillna(df_daily_energy[df_daily_energy>0].mean(), inplace=True)
    df_daily_energy_t = df_daily_energy.transpose()
    
    #%%
    # get meter usage data for one project
    
    y = []
    no_comm_meter = []
    
    for meter in np.array(meter_list_a):
        #print(meter)
        try: 
            df_daily_energy_t.loc[meter, :]
        except:
            print(meter+' has no communication')
            try: 
                no_comm_meter.append(meter)
            except:
                no_comm_meter = [meter]
            try:
                x = np.argwhere(meter_list_a==meter)
                y.append(x)
            except:
                x = np.argwhere(meter_list_a==meter)
                y = [x]
    
    meter_list_b = np.delete(meter_list_a, y)
    df_daily_energy_project = df_daily_energy_t.loc[meter_list_b,:]
    #df_daily_energy_project['meter number'] = df_daily_energy_project.index
    df_daily_energy_project_a = df_customer_project.join(df_daily_energy_project)
    df_daily_energy_project_a['meter number'] = df_daily_energy_project_a.index
    df_daily_energy_project_a.set_index('customer num', inplace=True)
    
    #%%
    # get customer useage data for one project
    df_customer_charge_pivot = df_customer_charge_pull.pivot(index='Account Num', columns='Date', values='Usage')
    df_customer_charge_project = df_customer_charge_pivot.loc[df_daily_energy_project_a.index, :]
    
    #%%
    # split project meter data into cust info & meter usage
    df_daily_project_usage = df_daily_energy_project_a.iloc[:, 12:-1]
    df_customer_info_a = df_daily_energy_project_a.iloc[:, 2:13]
    df_customer_info_a['meter number'] = df_daily_energy_project_a.iloc[:, -1]
    
    
    #%%
    # writew to excel
    file_folder = '../data/charges_meters/'
    file_name = project_name + ' usage.xlsx'
    writer = pd.ExcelWriter(file_folder+file_name, engine='openpyxl')
    df_customer_info_a.to_excel(writer, 'customer info')
    df_customer_charge_project.to_excel(writer, 'Customer Charge')
    df_daily_project_usage.to_excel(writer, 'Meter Usage')
    df_difference_daily = df_daily_project_usage-df_customer_charge_project
    df_difference_daily.to_excel(writer, 'charge v meter daily')
    
    #%% Monthly analysis
    df_daily_project_usage.columns = pd.to_datetime(df_daily_project_usage.columns)
    df_customer_charge_project.columns = pd.to_datetime(df_customer_charge_project.columns)
    df_meter_monthly = df_daily_project_usage.resample('M', axis=1).sum()
    df_customer_monthly = df_customer_charge_project.resample('M', axis=1).sum()
    
    df_meter_weekly = df_daily_project_usage.resample('w', axis=1).sum()
    df_customer_weekly = df_customer_charge_project.resample('w', axis=1).sum()

    df_difference_monthly = df_meter_monthly - df_customer_monthly 
    df_difference_weekly = df_meter_weekly - df_customer_weekly
    df_meter_monthly.to_excel(writer, 'monthly meter')
    df_customer_monthly.to_excel(writer, 'monthly cust')
    df_difference_monthly.to_excel(writer, 'charge v meter monthly')
    df_meter_weekly.to_excel(writer, 'weekly meter')
    df_customer_weekly.to_excel(writer, 'weekly cust')
    df_difference_weekly.to_excel(writer, 'charge v meter weekly')
    df_daily_project_usage['meter number'] = df_customer_info_a['meter number']
    
    writer.save()
# import the models needed
from pandas.io.parquet import get_engine
from pandas.tseries.offsets import Day
import psycopg2
import pandas as pd
import numpy as np
from datetime import date, timedelta, datetime
import os


pd.options.display.max_columns = None
today = date.today()
yesterday = today - timedelta(days=1)
print(os.getcwd())



df_meters_day = pd.read_csv(f'../data/meter_data/{today.strftime("%Y%m%d")}.csv', index_col='meter_id', parse_dates=['measured_at'])
df_meters_day_before = pd.read_csv(f'../data/meter_data/{yesterday.strftime("%Y%m%d")}.csv', index_col='meter_id', parse_dates=['measured_at'])
df_meters = df_meters_day.append(df_meters_day_before)



# prepare the sending email details
import os
import smtplib
import imghdr
from email.message import EmailMessage

EMAIL_ADDRESS = 'energi.city.corps@gmail.com'
EMAIL_PASSWORD = 'Ahmed.1996'

contacts = ['energi.city.corps@gmail.com']
msg =EmailMessage()


# the if statument check if we have a reading today from the cusotmer at 1 PM

# if we don't have any readings at 1 PM today it will send an email to Sanuratu to run Joe's script
if df_meters_day[df_meters_day['measured_at'].dt.hour == 13].shape[0] == 0:
  print(f"please run Joe script first because there isn't any readings at noon on {today}")

  # mak e a decision if you want to send an email to Sanurate
  want_to_email = input('(Yes) to send, (No) to not send: ')
  if want_to_email == 'yes' or want_to_email == 'YES' or want_to_email == 'y':
    msg['Subject'] = f'Running the meter script for {today}'
    msg['From'] = 'energi.city.corps@gmail.com'
    msg['To'] = 'ahmed.elsir@energicityconsulting.com'

    msg.set_content('This is a plain text email')

    msg.add_alternative(f"""\
    <!DOCTYPE html>
    <html>
        <body>
            <p>Hi Sanuratu</p>
            <p>Can you please run Joe&apos;s script for the meter for <strong>{today}&nbsp;</strong>because I&apos;m trying to get the noon to noon usage from it for some customers? and please let me know when it's done so I can run it again.</p>
            <p><br></p>
            <p>Best,</p>
            <p>Ahmed</p>
        </body>
    </html>
    """, subtype='html')
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        smtp.send_message(msg) 
  else:
    print("we're not going to send any messages")



# if there are readings from customers today at 1 PM it will process it and then send an email to Elorm, Kumba, Isata, and Ahmed.
else: 
  con = psycopg2.connect(database="energicity_production", user="read_only", password="vt39v62tz5toajs3", host="energicity-production-database-do-user-7511625-0.a.db.ondigitalocean.com", port="25060")

  print("Database opened successfully")

  all_customers = pd.read_sql('''
  select c.meter_id, c.id, c.first_name, c.last_name, p.name from customers c
  inner join projects p on c.project_id = p.id
  where p.country_code='SL';
  ''', con=con, index_col='meter_id')

  df_masimera = df_meters.join(all_customers)

  df_masimera = df_masimera.loc[df_masimera['name'] == 'Masimera']

  df_masimera = df_masimera[df_masimera.index.isin(['0013A20041ACFCEC', '0013A20041C53BCB', '0013A20041A77EFC', '0013A20041ACF71B', '0013A20041ACF405', '0013A20041ACF5CF', '0013A20041A77E91', '0013A20041C53BDC', '0013A20041A77F6B', '0013A20041BE9212'])]
  
  masimera_customers = df_masimera.groupby('meter_id')


  df_charges = pd.DataFrame(columns=['meter_id', 'customer_id', 'first_name', 'last_name', f'{today} usage', f'{yesterday} usage', 'charges'])

  for meter_id, df_customer in masimera_customers.__iter__():
    customers_billing = []
    customers_billing.extend([meter_id, df_customer.iloc[0]['id'], df_customer.iloc[0]['first_name'], df_customer.iloc[0]['last_name']])
    

    df_customer['hour'] = df_customer['measured_at'].dt.hour
    df_customer = df_customer.loc[df_customer['hour'] == 13]

    try:
      yesterday_usage = float("{:.5f}".format(df_customer.loc[df_customer['measured_at'].dt.date == yesterday].iloc[0]['usage_in_kwh']))
      print('yesterday usage', yesterday_usage)
    except:
      yesterday_usage = False

    try:
      today_usage = float("{:.5f}".format(df_customer.loc[df_customer['measured_at'].dt.date == today].iloc[0]['usage_in_kwh']))
      
    except:
      today_usage = False


    if today_usage and yesterday_usage:
      customers_billing.extend([today_usage, yesterday_usage, float("{:.5f}".format(today_usage - yesterday_usage))])
      df_charges.loc[len(df_charges)] = customers_billing

  KWH_COST = 6125
  df_charges['price'] = df_charges['charges'] * KWH_COST

  df_charges = df_charges.astype({"price": int})

  df_charges.set_index(keys=['meter_id'], inplace=True)



  msg['Subject'] = f'Noon to Noon for {today}'
  msg['From'] = 'energi.city.corps@gmail.com'
  msg['To'] = ['ahmed.elsir@energicityconsulting.com', 'kkasse-gborie@powerleone.com', 'emedenu@powerleone.com', 'ijalloh@powerleone.com', 'patrick.ayemeli@energicitycorp.com', 'ituray@powerleone.com']

  # msg['To'] = ['ahmed.elsir@energicityconsulting.com']

  msg.set_content('This is a plain text email')

  msg.add_alternative(f"""\
  <!DOCTYPE html>
  <html>
      <body>
      <p>
        Hi all, this is the noon to noon charges for {today}
      </p>
          {df_charges.to_html()}
      </body>
  </html>
  """, subtype='html')


  with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
      smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
      smtp.send_message(msg)  

